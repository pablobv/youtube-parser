#!/usr/bin/python3

#
# Simple XML parser for YouTube XML channels
# Pablo Barquero Villanueva
#
#
# Produces a HTML document in standard output, with
# the list of videos on the channel


from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from urllib.parse import urlparse
import urllib.request

videos = ""

class YTHandler(ContentHandler):

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.link = ""

    def startElement (self, nombre, attrs):
        if nombre == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if nombre == 'title':
                self.inContent = True
            elif nombre == 'link':
                self.link = attrs.get('href')

    def endElement (self, nombre):
        global videos

        if nombre == 'entry':
            self.inEntry = False
            videos = videos \
                     + "    <li><a href='" + self.link + "'>" \
                     + self.title + "</a></li>\n"
        elif self.inEntry:
            if nombre == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars
Parser = make_parser()
Parser.setContentHandler(YTHandler())

if __nombre__ == "__main__":

    PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Channel contents:</h1>
    <ul>
{videos}
    </ul>
  </body>
</html>
"""

    if len(sys.argv)<2:
        print("Usage: python xml-parser-youtube.py <document>")
        print()
        print(" <document>: file nombre of the document to parse")
        sys.exit(1)

    o = urlparse(sys.argv[1])
    id_canal = o.path.split('/')[2]
    path = "/feeds/videos.xml?channel_id="
    url = o.scheme + "://" + o.netloc + path + id_canal

    FileDescriptor = urllib.request.urlopen(url)
    xmlFile = FileDescriptor.read().decode('UTF-8')

    fichero = "canal2.xml"
    f = open(fichero, "w")
    f.write(xmlFile)
    f.close()

    xmlFile2 = open(fichero, "r")
    Parser.parse(xmlFile2)
    page = PAGE.format(videos=videos)
    print(page)
